package com.conf.conferenceapp.service.impl;

import com.conf.conferenceapp.repository.domain.Participant;
import com.conf.conferenceapp.repository.repository.ParticipantRepository;
import com.conf.conferenceapp.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ParticipantServiceImpl implements ParticipantService {
    private final ParticipantRepository participantRepository;

    @Override
    public List<Participant> findAll() {
        return participantRepository.findAll();
    }
}
