package com.conf.conferenceapp.service.impl;

import com.conf.conferenceapp.repository.domain.Conference;
import com.conf.conferenceapp.repository.domain.Participant;
import com.conf.conferenceapp.repository.repository.ConferenceRepository;
import com.conf.conferenceapp.repository.repository.ParticipantRepository;
import com.conf.conferenceapp.service.ConferenceService;
import com.conf.conferenceapp.service.exception.BusinessException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConferenceServiceImpl implements ConferenceService {
    private final ConferenceRepository conferenceRepository;
    private final ParticipantRepository participantRepository;

    public Conference save(Conference conference) {
        return conferenceRepository.save(conference);
    }

    @Override
    public List<Conference> findAll() {
        return conferenceRepository.findAll();
    }

    @Override
    public Conference register(Participant participant, Integer conferenceId) throws BusinessException {
        Conference conference = Optional.of(conferenceRepository.getById(conferenceId))
                .orElseThrow(() -> new BusinessException("Conference with given ID not found: " + conferenceId));
        if (conference.getParticipantCounter() + 1 > conference.getParticipantLimit())
            throw new BusinessException("Conference participants limit exceeded: " + conference.getParticipantLimit());
        conference.setParticipantCounter(conference.getParticipantCounter() + 1);
        participant.setConference(conference);
        conferenceRepository.save(conference);
        participantRepository.save(participant);
        return conference;
    }
}
