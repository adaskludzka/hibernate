package com.conf.conferenceapp.service;

import com.conf.conferenceapp.repository.domain.Conference;
import com.conf.conferenceapp.repository.domain.Participant;
import com.conf.conferenceapp.service.exception.BusinessException;

import java.util.List;

public interface ConferenceService {
    Conference save(Conference conference);

    List<Conference> findAll();

    Conference register(Participant participant, Integer conferenceId) throws BusinessException;
}
