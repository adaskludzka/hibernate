package com.conf.conferenceapp.service;

import com.conf.conferenceapp.repository.domain.Participant;

import java.util.List;

public interface ParticipantService {
    List<Participant> findAll();
}
