package com.conf.conferenceapp.service.exception;

public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }
}
