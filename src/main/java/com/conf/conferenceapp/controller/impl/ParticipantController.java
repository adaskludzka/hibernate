package com.conf.conferenceapp.controller.impl;

import com.conf.conferenceapp.controller.ParticipantControllerApi;
import com.conf.conferenceapp.repository.domain.Participant;
import com.conf.conferenceapp.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ParticipantController implements ParticipantControllerApi {
    private final ParticipantService participantService;

    @Override
    public ResponseEntity<List<Participant>> findAll() {
        return ResponseEntity.ok(participantService.findAll());
    }
}
