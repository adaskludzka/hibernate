package com.conf.conferenceapp.controller.impl;

import com.conf.conferenceapp.controller.ConferenceControllerApi;
import com.conf.conferenceapp.repository.domain.Conference;
import com.conf.conferenceapp.repository.domain.Participant;
import com.conf.conferenceapp.service.ConferenceService;
import com.conf.conferenceapp.service.exception.BusinessException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ConferenceController implements ConferenceControllerApi {
    private final ConferenceService conferenceService;

    @Override
    public ResponseEntity<Conference> save(Conference conference) {
        return ResponseEntity.ok(conferenceService.save(conference));
    }

    @Override
    public ResponseEntity<List<Conference>> findAll() {
        return ResponseEntity.ok(conferenceService.findAll());
    }

    @Override
    public ResponseEntity<Conference> addParticipant(Participant participant, Integer conferenceId) throws BusinessException {
        return ResponseEntity.ok(conferenceService.register(participant, conferenceId));
    }
}
