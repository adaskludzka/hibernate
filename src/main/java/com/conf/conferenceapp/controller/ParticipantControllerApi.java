package com.conf.conferenceapp.controller;

import com.conf.conferenceapp.repository.domain.Participant;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping(value = "/api/participant", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ParticipantControllerApi {
    @GetMapping
    ResponseEntity<List<Participant>> findAll();
}
