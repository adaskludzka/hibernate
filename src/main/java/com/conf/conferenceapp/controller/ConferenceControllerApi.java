package com.conf.conferenceapp.controller;

import com.conf.conferenceapp.repository.domain.Conference;
import com.conf.conferenceapp.repository.domain.Participant;
import com.conf.conferenceapp.service.exception.BusinessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/api/conference", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ConferenceControllerApi {
    @PostMapping
    ResponseEntity<Conference> save(@RequestBody Conference conference);

    @GetMapping
    ResponseEntity<List<Conference>> findAll();

    @PostMapping("/register/{conferenceId}")
    ResponseEntity<Conference> addParticipant(@RequestBody Participant participant, @PathVariable Integer conferenceId) throws BusinessException;
}
