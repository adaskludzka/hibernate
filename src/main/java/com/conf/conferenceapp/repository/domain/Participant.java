package com.conf.conferenceapp.repository.domain;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "conference")
@Entity
@SequenceGenerator(name = "seq_participant", sequenceName = "seq_participant", allocationSize = 1)
public class Participant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_participant")
    private Integer id;

    @Column
    private String name;

    @Column
    private String surname;

    @Column
    private Integer age;

    @Column
    private String industry;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_conference")
    private Conference conference;
}
