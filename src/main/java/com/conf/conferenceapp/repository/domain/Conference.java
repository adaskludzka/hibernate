package com.conf.conferenceapp.repository.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@SequenceGenerator(name = "seq_conference", sequenceName = "seq_conference", allocationSize = 1)
public class Conference {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_conference")
    private Integer id;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private Integer participantLimit = 0;

    @Column
    private Integer participantCounter = 0;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;
}
