package com.conf.conferenceapp.repository.repository;

import com.conf.conferenceapp.repository.domain.Conference;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConferenceRepository extends JpaRepository<Conference, Integer> {
}
