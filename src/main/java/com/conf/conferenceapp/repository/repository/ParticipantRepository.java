package com.conf.conferenceapp.repository.repository;

import com.conf.conferenceapp.repository.domain.Participant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticipantRepository extends JpaRepository<Participant, Integer> {
}
